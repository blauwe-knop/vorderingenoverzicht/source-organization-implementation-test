FROM golang:1.23.1-alpine AS build

COPY ./cmd /go/src/source_organization_implementation_test/cmd
COPY ./internal /go/src/source_organization_implementation_test/internal
COPY ./pkg /go/src/source_organization_implementation_test/pkg
COPY ./go.mod /go/src/source_organization_implementation_test/
COPY ./go.sum /go/src/source_organization_implementation_test/

RUN apk add --update --no-cache git
WORKDIR /go/src/source_organization_implementation_test
RUN go mod download \
 && go build -o dist/bin/source_organization_implementation_test ./cmd/source_organization_implementation_test

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/source_organization_implementation_test/dist/bin/source_organization_implementation_test /usr/local/bin/source_organization_implementation_test

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/source_organization_implementation_test"]
