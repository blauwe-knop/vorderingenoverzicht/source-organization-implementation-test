// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"

	"go.uber.org/zap"

	usecases "gitlab.com/blauwe-knop/vorderingenoverzicht/source-organization-implementation-test/pkg/usecase"
)

type options struct {
	DiscoveryUrl         string `long:"discovery-url" env:"DISCOVERY_URL" description:"discovery url."`
	Bsn                  string `long:"bsn" env:"BSN" description:"BSN"`
	PublicKey            string `long:"public-key" env:"PUBLIC_KEY" description:"Organization public key"`
	SchemeDiscoveryUrl   string `long:"scheme-discovery-url" env:"SCHEME_DISCOVERY_URL" description:"scheme discovery url."`
	AppManagerOin        string `long:"app-manager-oin" env:"APP_MANAGER_OIN" description:"app manager oin"`
	AppManagerPrivateKey string `long:"app-manager-private-key" env:"APP_MANAGER_PRIVATE_KEY" description:"app manager private key"`
	Online               bool   `long:"online" env:"ONLINE" description:"Check if endpoints are online"`
	HealthCheck          bool   `long:"health-check" env:"HEALTH_CHECK" description:"Check if endpoints are working"`
	FunctionalOffline    bool   `long:"functional-offline" env:"FUNCTIONAL_OFFLINE" description:"Functional offline test"`
	FunctionalOnline     bool   `long:"functional-online" env:"FUNCTIONAL_ONLINE" description:"Functional online test"`

	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	if len(cliOptions.DiscoveryUrl) < 1 {
		log.Fatalf("please specify a discovery-url")
	}

	if cliOptions.FunctionalOffline {
		if len(cliOptions.Bsn) < 1 {
			log.Fatalf("please specify a bsn")
		}

		if len(cliOptions.PublicKey) < 1 {
			log.Fatalf("please specify a public-key")
		}

		if len(cliOptions.SchemeDiscoveryUrl) < 1 {
			log.Fatalf("please specify a scheme-discovery-url")
		}

		if len(cliOptions.AppManagerOin) < 1 {
			log.Fatalf("please specify a app-manager-oin")
		}

		if len(cliOptions.AppManagerPrivateKey) < 1 {
			log.Fatalf("please specify a app-manager-private-key")
		}
	}

	if cliOptions.FunctionalOnline {
		if len(cliOptions.Bsn) < 1 {
			log.Fatalf("please specify a bsn")
		}

		if len(cliOptions.PublicKey) < 1 {
			log.Fatalf("please specify a public-key")
		}

		if len(cliOptions.SchemeDiscoveryUrl) < 1 {
			log.Fatalf("please specify a scheme-discovery-url")
		}

		if len(cliOptions.AppManagerOin) < 1 {
			log.Fatalf("please specify a app-manager-oin")
		}
	}

	testAll := !cliOptions.Online && !cliOptions.HealthCheck && !cliOptions.FunctionalOffline && !cliOptions.FunctionalOnline

	sourceOrganizationImplementationTestUsecase := usecases.NewSourceOrganizationImplementationTestUsecase(
		logger,
	)

	if cliOptions.Online || testAll {
		logger.Info("--- Online: ---")
		errorList := sourceOrganizationImplementationTestUsecase.OnlineTest(
			cliOptions.DiscoveryUrl,
		)
		if len(errorList) > 0 {
			logger.Fatal("--- Online: FAILED ---", zap.Errors("errorList", errorList))
		} else {
			logger.Info("--- Online: SUCCEEDED ---")
		}
	}

	if cliOptions.HealthCheck || testAll {
		logger.Info("--- Health check: ---")
		errorList := sourceOrganizationImplementationTestUsecase.HealthCheckTest(
			cliOptions.DiscoveryUrl,
		)
		if len(errorList) > 0 {
			logger.Fatal("--- Health check: FAILED ---", zap.Errors("errorList", errorList))
		} else {
			logger.Info("--- Health check: SUCCEEDED ---")
		}
	}

	if cliOptions.FunctionalOffline || testAll {
		logger.Info("--- Functional offline test: ---")
		err := sourceOrganizationImplementationTestUsecase.FunctionalOfflineTest(
			cliOptions.DiscoveryUrl,
			cliOptions.Bsn,
			cliOptions.PublicKey,
			cliOptions.SchemeDiscoveryUrl,
			cliOptions.AppManagerOin,
			cliOptions.AppManagerPrivateKey,
		)
		if err != nil {
			logger.Fatal("--- Functional offline test: FAILED ---", zap.Error(err))
		} else {
			logger.Info("--- Functional offline test: SUCCEEDED ---")
		}
	}

	if cliOptions.FunctionalOnline || testAll {
		logger.Info("--- Functional online test: ---")
		err := sourceOrganizationImplementationTestUsecase.FunctionalOnlineTest(
			cliOptions.DiscoveryUrl,
			cliOptions.Bsn,
			cliOptions.PublicKey,
			cliOptions.SchemeDiscoveryUrl,
			cliOptions.AppManagerOin,
		)
		if err != nil {
			logger.Fatal("--- Functional online test: FAILED ---", zap.Error(err))
		} else {
			logger.Info("--- Functional online test: SUCCEEDED ---")
		}
	}
}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
