# Source organization implementation test

The `source-organization-implementation-test` can be configured with environment variables/parameters.

| ENVIRONMENT             | Parameter               | Description                                                                                   | Required for functional offline test | Required for functional online test |
| ----------------------- | ----------------------- | --------------------------------------------------------------------------------------------- | ------------------------------------ | ----------------------------------- |
| DISCOVERY_URL           | discovery-url           | The url of source organization discovery (process compnent `service-discovery-process`).      | ✅                                   | ✅                                  |
| BSN                     | bsn                     | BSN of the financial information claim document to be requested.                              | ✅                                   | ✅                                  |
| PUBLIC_KEY              | public-key              | Public key of the source organization.                                                        | ✅                                   | ✅                                  |
| SCHEME_DISCOVERY_URL    | scheme-discovery-url    | The discovery url of the scheme api.                                                          | ✅                                   | ✅                                  |
| APP_MANAGER_OIN         | app-manager-oin         | The oin of the app manager.                                                                   | ✅                                   | ✅                                  |
| APP_MANAGER_PRIVATE_KEY | app-manager-private-key | The private key of the app manager.                                                           | ✅                                   | ❌                                  |
| ONLINE                  | online                  | Testflag to check if endpoints are online.                                                    | -                                    | -                                   |
| HEALTH_CHECK            | health-check            | Testflag to do a health check on endpoints.                                                   | -                                    | -                                   |
| FUNCTIONAL_OFFLINE      | functional-offline      | Testflag to execute the offline functional test. (without an App Manager)                     | -                                    | -                                   |
| FUNCTIONAL_ONLINE       | functional-online       | Testflag to execute the online functional test. (with an App Manager)                         | -                                    | -                                   |
| LOG_TYPE                | log-type                | Set the logging config. For different outputs default:"test", other options: "live", "local". | -                                    | -                                   |
| LOG_LEVEL               | LOG_LEVEL               | Override the default loglevel default:"info" other options: "debug", "warn".                  | -                                    | -                                   |

If none of the Testflags has been set, all the tests will be executed.

An example helm chart is available in the `helm` folder

Other examples to run the `source-organization-implementation-test` for `docker run`, `go run` and `skaffold` can be found in [EXAMPLES.md](EXAMPLES.md)

## Run functional offline test with mock scheme manager with helm

1. Deploy CloudNativePG

   ```sh
   helm repo add cnpg https://cloudnative-pg.github.io/charts

   helm upgrade --install cnpg \
   --namespace cnpg-system \
   --create-namespace \
   cnpg/cloudnative-pg
   ```

1. Make sure your source organization has the same `schemeDiscoveryUrl` set in `citizen-financial-claims-process` as the variable `config.hostAdress` for `mock-scheme-process` underneath with protocol and version number in path, in this example `http://api.mock-stelsel.bk/v1`.

1. Deploy scheme manager

   ```sh
   helm repo add vorijk https://blauwe-knop.gitlab.io/vorderingenoverzicht/charts

   helm upgrade --install mock-scheme-db \
      --create-namespace \
      --namespace bk-scheme-manager \
      --version 0.16.1 \
      vorijk/scheme-db

   helm upgrade --install mock-scheme-service \
      --namespace bk-scheme-manager \
      --set config.containerPort=8008 \
      --version 0.16.0 \
      vorijk/scheme-service

   helm upgrade --install mock-scheme-process \
      --namespace bk-scheme-manager \
      --set config.hostAddress=api.mock-stelsel.bk \
      --set config.containerPort=8008 \
      --set config.appManagerDiscoveryUrl="" \
      --set config.appManagerPublicKey="-----BEGIN PUBLIC KEY-----
   MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80
   AoHZKXHK3JL8sqBUbYlC6MoGh0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
   -----END PUBLIC KEY-----" \
      --version 0.16.0 \
      vorijk/scheme-process
   ```

1. Generate key pair on bk management ui of your source organization. ("Blauwe Knop Bronorganisatiebeheer"), replace the value of the variable `config.publicKey`, in the script below, to your source organizations public key. Which can be found on the same screen after generating.

1. Deploy source organization implementation test

   ```sh
   helm upgrade --install source-organization-implementation-test \
      --namespace bk-source-organization-implementation-test \
      --create-namespace \
      --set config.discoveryUrl="http://service-discovery-api.demo.bk-manager.source-organization.vorderingenoverzicht.blauweknop.bk/.well-known/bk-configuration.json" \
      --set config.bsn="814859094" \
      --set config.publicKey="-----BEGIN RSA PUBLIC KEY-----
   MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEhvEq610HdunQ+YSGHV2PYuyTlkzw
   uqBWBN+R4pIgazDLXQ2tcAV6XB8GHICpQsLKNupmm2vj6RZ2EBXOJaw0sw==
   -----END RSA PUBLIC KEY-----" \
      --set config.schemeDiscoveryUrl="http://api.mock-stelsel.bk/v1" \
      --set config.appManagerOin="00000001001172773000" \
      --set config.appManagerPrivateKey="-----BEGIN EC PRIVATE KEY-----
   MHcCAQEEIL+WYjDkIULn/pSZGgQDApAYA3kk5ZYkKUfwD6WVHbkeoAoGCCqGSM49
   AwEHoUQDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80AoHZKXHK3JL8sqBUbYlC6MoG
   h0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
   -----END EC PRIVATE KEY-----" \
      --set config.online="true" \
      --set config.healthCheck="true" \
      --set config.functionalOffline="true" \
      --version 0.16.0 \
      vorijk/source-organization-implementation-test
   ```

## Cleanup

Delete namespace

```sh
kubectl delete namespace bk-scheme-manager
kubectl delete namespace bk-source-organization-implementation-test
kubectl delete namespace cnpg-system
```
