module gitlab.com/blauwe-knop/vorderingenoverzicht/source-organization-implementation-test

go 1.22.3

require (
	github.com/golang-jwt/jwt/v5 v5.2.1
	github.com/jessevdk/go-flags v1.6.1
	github.com/thessem/zap-prettyconsole v0.5.2
	gitlab.com/blauwe-knop/common/health-checker v0.0.8
	gitlab.com/blauwe-knop/connect/go-connect v1.0.9
	gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process v0.18.20
	gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service v0.17.13
	gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process v0.18.18
	gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system v0.19.17
	gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process v0.17.16
	gitlab.com/blauwe-knop/vorderingenoverzicht/service-discovery-process v0.17.16
	gitlab.com/blauwe-knop/vorderingenoverzicht/session-process v0.17.18
	go.uber.org/zap v1.27.0
)

//OWASP scan override of indirect dependencies:
require github.com/hashicorp/go-retryablehttp v0.7.7

require (
	github.com/Code-Hex/dd v1.1.0 // indirect
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.3.0 // indirect
	github.com/ethereum/go-ethereum v1.14.12 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/holiman/uint256 v1.3.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service v0.17.13 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/crypto v0.32.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
)
