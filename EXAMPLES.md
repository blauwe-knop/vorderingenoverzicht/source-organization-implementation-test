# Examples

## Examples functional online test

### Docker test environment functional online test

```sh
docker run \
   -e DISCOVERY_URL="https://bd.vorijk-test.blauweknop.app/.well-known/bk-configuration.json" \
   -e BSN="814859094" \
   -e PUBLIC_KEY="-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEhvEq610HdunQ+YSGHV2PYuyTlkzw
uqBWBN+R4pIgazDLXQ2tcAV6XB8GHICpQsLKNupmm2vj6RZ2EBXOJaw0sw==
-----END PUBLIC KEY-----" \
   -e SCHEME_DISCOVERY_URL="http://api.stelsel.vorijk-test.blauweknop.app/v1" \
   -e APP_MANAGER_OIN="00000001001172773000" \
   -e ONLINE \
   -e HEALTH_CHECK \
   -e FUNCTIONAL_ONLINE \
   registry.gitlab.com/blauwe-knop/vorderingenoverzicht/source-organization-implementation-test
```

### Docker demo environment functional online test

```sh
docker run \
   -e DISCOVERY_URL="https://bd.vorijk-demo.blauweknop.app/.well-known/bk-configuration.json" \
   -e BSN="814859094" \
   -e PUBLIC_KEY="-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEhvEq610HdunQ+YSGHV2PYuyTlkzw
uqBWBN+R4pIgazDLXQ2tcAV6XB8GHICpQsLKNupmm2vj6RZ2EBXOJaw0sw==
-----END PUBLIC KEY-----" \
   -e SCHEME_DISCOVERY_URL="http://api.stelsel.vorijk-demo.blauweknop.app/v1" \
   -e APP_MANAGER_OIN="00000001001172773000" \
   -e ONLINE \
   -e HEALTH_CHECK \
   -e FUNCTIONAL_ONLINE \
   registry.gitlab.com/blauwe-knop/vorderingenoverzicht/source-organization-implementation-test
```

### Docker local functional online test

```sh
docker run \
   -e DISCOVERY_URL="http://service-discovery-api.demo.bk-manager.source-organization.vorderingenoverzicht.blauweknop.bk/.well-known/bk-configuration.json" \
   -e BSN="814859094" \
   -e PUBLIC_KEY="-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEwVuAO1TMxBE+b5Bbs0OSm3sy5D+1
5095jPfIW7OaEqGGvl1yV7XlB3nhsDaGrLV6yi7eqZcE6CvP4PUsNn3ICQ==
-----END PUBLIC KEY-----" \
   -e SCHEME_DISCOVERY_URL="https://api.stelsel.vorijk-demo.blauweknop.app/v1" \
   -e APP_MANAGER_OIN="00000001001172773000" \
   -e ONLINE \
   -e HEALTH_CHECK \
   -e FUNCTIONAL_ONLINE \
   registry.gitlab.com/blauwe-knop/vorderingenoverzicht/source-organization-implementation-test
```

### Go test environment functional online test

```sh
go run cmd/source_organization_implementation_test/*.go \
   --discovery-url "https://bd.vorijk-test.blauweknop.app/.well-known/bk-configuration.json" \
   --bsn "814859094" \
   --public-key "-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEhvEq610HdunQ+YSGHV2PYuyTlkzw
uqBWBN+R4pIgazDLXQ2tcAV6XB8GHICpQsLKNupmm2vj6RZ2EBXOJaw0sw==
-----END PUBLIC KEY-----" \
   --scheme-discovery-url "https://api.stelsel.vorijk-test.blauweknop.app/v1" \
   --app-manager-oin "00000001001172773000" \
   --online \
   --health-check \
   --functional-online
```

### Go demo environment functional online test

```sh
go run cmd/source_organization_implementation_test/*.go \
   --discovery-url "https://bd.vorijk-demo.blauweknop.app/.well-known/bk-configuration.json" \
   --bsn "814859094" \
   --public-key "-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEhvEq610HdunQ+YSGHV2PYuyTlkzw
uqBWBN+R4pIgazDLXQ2tcAV6XB8GHICpQsLKNupmm2vj6RZ2EBXOJaw0sw==
-----END PUBLIC KEY-----" \
   --scheme-discovery-url "https://api.stelsel.vorijk-demo.blauweknop.app/v1" \
   --app-manager-oin "00000001001172773000" \
   --online \
   --health-check \
   --functional-online
```

### Go local functional online test

```sh
go run cmd/source_organization_implementation_test/*.go \
   --discovery-url "http://service-discovery-api.demo.bk-manager.source-organization.vorderingenoverzicht.blauweknop.bk/.well-known/bk-configuration.json" \
   --bsn "814859094" \
   --public-key "-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEwVuAO1TMxBE+b5Bbs0OSm3sy5D+1
5095jPfIW7OaEqGGvl1yV7XlB3nhsDaGrLV6yi7eqZcE6CvP4PUsNn3ICQ==
-----END PUBLIC KEY-----" \
   --scheme-discovery-url "http://scheme-process.scheme-manager.vorderingenoverzicht.blauweknop.bk/v1" \
   --app-manager-oin "00000001001172773000" \
   --online \
   --health-check \
   --functional-online
```

### Skaffold local functional online test

Create namespace

```sh
kubectl create namespace bk-source-organization-implementation-test
```

Run skaffold

```sh
skaffold dev -f=skaffold.online.yaml
```

Delete namespace

```sh
kubectl delete namespace bk-source-organization-implementation-test
```

## Examples functional offline test

### Docker test environment functional offline test

```sh
docker run \
   -e DISCOVERY_URL="https://bd.vorijk-test.blauweknop.app/.well-known/bk-configuration.json" \
   -e BSN="814859094" \
   -e PUBLIC_KEY="-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEhvEq610HdunQ+YSGHV2PYuyTlkzw
uqBWBN+R4pIgazDLXQ2tcAV6XB8GHICpQsLKNupmm2vj6RZ2EBXOJaw0sw==
-----END PUBLIC KEY-----" \
   -e SCHEME_DISCOVERY_URL="http://api.stelsel.vorijk-test.blauweknop.app/v1" \
   -e APP_MANAGER_OIN="00000001001172773000" \
   -e APP_MANAGER_PRIVATE_KEY="-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIL+WYjDkIULn/pSZGgQDApAYA3kk5ZYkKUfwD6WVHbkeoAoGCCqGSM49
AwEHoUQDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80AoHZKXHK3JL8sqBUbYlC6MoG
h0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END EC PRIVATE KEY-----" \
   -e ONLINE \
   -e HEALTH_CHECK \
   -e FUNCTIONAL_OFFLINE \
   registry.gitlab.com/blauwe-knop/vorderingenoverzicht/source-organization-implementation-test
```

### Docker demo environment functional offline test

```sh
docker run \
   -e DISCOVERY_URL="https://bd.vorijk-demo.blauweknop.app/.well-known/bk-configuration.json" \
   -e BSN="814859094" \
   -e PUBLIC_KEY="-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEhvEq610HdunQ+YSGHV2PYuyTlkzw
uqBWBN+R4pIgazDLXQ2tcAV6XB8GHICpQsLKNupmm2vj6RZ2EBXOJaw0sw==
-----END PUBLIC KEY-----" \
   -e SCHEME_DISCOVERY_URL="http://api.stelsel.vorijk-test.blauweknop.app/v1" \
   -e APP_MANAGER_OIN="00000001001172773000" \
   -e APP_MANAGER_PRIVATE_KEY="-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIL+WYjDkIULn/pSZGgQDApAYA3kk5ZYkKUfwD6WVHbkeoAoGCCqGSM49
AwEHoUQDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80AoHZKXHK3JL8sqBUbYlC6MoG
h0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END EC PRIVATE KEY-----" \
   -e ONLINE \
   -e HEALTH_CHECK \
   -e FUNCTIONAL_OFFLINE \
   registry.gitlab.com/blauwe-knop/vorderingenoverzicht/source-organization-implementation-test
```

### Docker local functional offline test

```sh
docker run \
   -e DISCOVERY_URL="http://service-discovery-api.demo.bk-manager.source-organization.vorderingenoverzicht.blauweknop.bk/.well-known/bk-configuration.json" \
   -e BSN="814859094" \
   -e PUBLIC_KEY="-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEwVuAO1TMxBE+b5Bbs0OSm3sy5D+1
5095jPfIW7OaEqGGvl1yV7XlB3nhsDaGrLV6yi7eqZcE6CvP4PUsNn3ICQ==
-----END PUBLIC KEY-----" \
   -e SCHEME_DISCOVERY_URL="https://api.stelsel.vorijk-demo.blauweknop.app/v1" \
   -e APP_MANAGER_OIN="00000001001172773000" \
   -e APP_MANAGER_PRIVATE_KEY="-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIL+WYjDkIULn/pSZGgQDApAYA3kk5ZYkKUfwD6WVHbkeoAoGCCqGSM49
AwEHoUQDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80AoHZKXHK3JL8sqBUbYlC6MoG
h0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END EC PRIVATE KEY-----" \
   -e ONLINE \
   -e HEALTH_CHECK \
   -e FUNCTIONAL_OFFLINE \
   registry.gitlab.com/blauwe-knop/vorderingenoverzicht/source-organization-implementation-test
```

### Go test environment functional offline test

```sh
go run cmd/source_organization_implementation_test/*.go \
   --discovery-url "https://bd.vorijk-test.blauweknop.app/.well-known/bk-configuration.json" \
   --bsn "814859094" \
   --public-key "-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEhvEq610HdunQ+YSGHV2PYuyTlkzw
uqBWBN+R4pIgazDLXQ2tcAV6XB8GHICpQsLKNupmm2vj6RZ2EBXOJaw0sw==
-----END PUBLIC KEY-----" \
   --scheme-discovery-url "https://api.stelsel.vorijk-test.blauweknop.app/v1" \
   --app-manager-oin "00000001001172773000" \
   --app-manager-private-key "-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIL+WYjDkIULn/pSZGgQDApAYA3kk5ZYkKUfwD6WVHbkeoAoGCCqGSM49
AwEHoUQDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80AoHZKXHK3JL8sqBUbYlC6MoG
h0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END EC PRIVATE KEY-----" \
   --online \
   --health-check \
   --functional-offline
```

### Go demo environment functional offline test

```sh
go run cmd/source_organization_implementation_test/*.go \
   --discovery-url "https://bd.vorijk-demo.blauweknop.app/.well-known/bk-configuration.json" \
   --bsn "814859094" \
   --public-key "-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEhvEq610HdunQ+YSGHV2PYuyTlkzw
uqBWBN+R4pIgazDLXQ2tcAV6XB8GHICpQsLKNupmm2vj6RZ2EBXOJaw0sw==
-----END PUBLIC KEY-----" \
   --scheme-discovery-url "https://api.stelsel.vorijk-demo.blauweknop.app/v1" \
   --app-manager-oin "00000001001172773000" \
   --app-manager-private-key "-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIL+WYjDkIULn/pSZGgQDApAYA3kk5ZYkKUfwD6WVHbkeoAoGCCqGSM49
AwEHoUQDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80AoHZKXHK3JL8sqBUbYlC6MoG
h0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END EC PRIVATE KEY-----" \
   --online \
   --health-check \
   --functional-offline
```

### Go local functional offline test

```sh
go run cmd/source_organization_implementation_test/*.go \
   --discovery-url "http://service-discovery-api.demo.bk-manager.source-organization.vorderingenoverzicht.blauweknop.bk/.well-known/bk-configuration.json" \
   --bsn "814859094" \
   --public-key "-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEwVuAO1TMxBE+b5Bbs0OSm3sy5D+1
5095jPfIW7OaEqGGvl1yV7XlB3nhsDaGrLV6yi7eqZcE6CvP4PUsNn3ICQ==
-----END PUBLIC KEY-----" \
   --scheme-discovery-url "http://scheme-process.scheme-manager.vorderingenoverzicht.blauweknop.bk/v1" \
   --app-manager-oin "00000001001172773000" \
   --app-manager-private-key "-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIL+WYjDkIULn/pSZGgQDApAYA3kk5ZYkKUfwD6WVHbkeoAoGCCqGSM49
AwEHoUQDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80AoHZKXHK3JL8sqBUbYlC6MoG
h0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
-----END EC PRIVATE KEY-----" \
   --online \
   --health-check \
   --functional-offline
```

### Skaffold local functional offline test

Create namespace

```sh
kubectl create namespace bk-source-organization-implementation-test
```

Run skaffold

```sh
skaffold dev -f=skaffold.offline.yaml
```

Delete namespace

```sh
kubectl delete namespace bk-source-organization-implementation-test
```
