// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package helpers

import (
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"
	appManagementProcessModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/model"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/source-organization-implementation-test/pkg/repository_log_wrappers"
)

func StartSession(
	cryptographyTestHelper *CryptographyTestHelper,
	sessionTestHelper *repository_log_wrappers.SessionTestHelper,
	appPublicKey ec.PublicKey,
	appPrivateKey ec.PrivateKey,
	sessionAesKey []byte,
	organizationPublicKey ec.PublicKey,
	certificate *appManagementProcessModel.Certificate,
) (string, error) {
	challenge, err := sessionTestHelper.RequestChallenge()
	if err != nil {
		return "", err
	}

	err = cryptographyTestHelper.VerifyNonce(
		*challenge,
		organizationPublicKey,
	)
	if err != nil {
		return "", err
	}

	signature, err := cryptographyTestHelper.SignNonce(
		challenge.Nonce,
		appPrivateKey,
	)
	if err != nil {
		return "", err
	}

	appPublicKeyPem, err := appPublicKey.ToPem()
	if err != nil {
		return "", err
	}

	challengeResult := model.ChallengeResult{
		AppNonceSignature: signature,
		AppPublicKey:      string(appPublicKeyPem),
		CertificateType:   certificate.Type,
		Certificate:       string(certificate.Value),
		SessionAesKey:     base64.ParseFromBytes(sessionAesKey),
	}

	ephemeralOrganizationEcPublicKey, err := ec.ParsePublicKeyFromPem([]byte(challenge.EphemeralOrganizationEcPublicKey))
	if err != nil {
		return "", err
	}

	encryptedChallengeResult, err := cryptographyTestHelper.EncryptChallengeResult(
		challengeResult,
		*ephemeralOrganizationEcPublicKey,
	)
	if err != nil {
		return "", err
	}

	challengeResponse := model.ChallengeResponse{
		Nonce:                    challenge.Nonce,
		EncryptedChallengeResult: encryptedChallengeResult,
	}

	encryptedSessionToken, err := sessionTestHelper.CompleteChallenge(
		challengeResponse,
	)
	if err != nil {
		return "", err
	}

	sessionToken, err := cryptographyTestHelper.DecryptSessionToken(sessionAesKey, encryptedSessionToken)
	if err != nil {
		return "", err
	}

	return sessionToken, nil
}
