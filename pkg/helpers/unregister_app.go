// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package helpers

import (
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ec"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/source-organization-implementation-test/pkg/repository_log_wrappers"
)

func UnregisterApp(
	appManagementRepositoryLogWrapper *repository_log_wrappers.AppManagementRepositoryLogWrapper,
	cryptographyTestHelper *CryptographyTestHelper,
	appManagerSessionToken string,
	appKeyPair ec.PrivateKey,
	registrationToken string,
) error {
	signature, err := cryptographyTestHelper.SignRegistrationToken(
		registrationToken,
		appKeyPair,
	)
	if err != nil {
		return err
	}

	unregisterAppRequest := model.UnregisterAppRequest{
		RegistrationToken: registrationToken,
		Signature:         string(signature),
	}

	err = appManagementRepositoryLogWrapper.UnregisterApp(
		appManagerSessionToken,
		unregisterAppRequest,
	)
	if err != nil {
		return err
	}

	return err
}
