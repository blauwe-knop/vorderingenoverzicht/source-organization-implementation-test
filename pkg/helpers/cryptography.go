// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package helpers

import (
	"crypto/elliptic"
	"encoding/json"
	"fmt"

	"gitlab.com/blauwe-knop/connect/go-connect/crypto/aes"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ecdsa"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ecies"
	"gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"
	appManagementProcessModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"
	citizenFinancialClaimModel "gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/model"
	sourceSystemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
	sessionModel "gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/model"

	"go.uber.org/zap"
)

type CryptographyTestHelper struct {
	Logger *zap.Logger
}

func NewCryptographyTestHelper(logger *zap.Logger) *CryptographyTestHelper {
	return &CryptographyTestHelper{
		Logger: logger,
	}
}

func (uc *CryptographyTestHelper) GenerateEcKeyPair() (*ec.PrivateKey, error) {
	uc.Logger.Debug("generate key pair")
	privateKey, err := ec.GenerateKeyPair(elliptic.P256())
	if err != nil {
		uc.Logger.Error("generate key pair failed", zap.Error(err))
		return nil, fmt.Errorf("generate key pair failed: %v", err)
	}

	privateKeyPem, err := privateKey.ToPem()
	if err != nil {
		uc.Logger.Error("private key to pem failed", zap.Error(err))
		return nil, fmt.Errorf("private key to pem failed: %v", err)
	}

	publicKeyPem, err := privateKey.PublicKey.ToPem()
	if err != nil {
		uc.Logger.Error("public key to pem failed", zap.Error(err))
		return nil, fmt.Errorf("public key to pem failed: %v", err)
	}

	uc.Logger.Info("generate key pair succeeded", zap.String("privateKey", string(privateKeyPem)), zap.String("publicKey", string(publicKeyPem)))

	return privateKey, nil
}

func (uc *CryptographyTestHelper) GenerateAesKeyPair() ([]byte, error) {
	uc.Logger.Debug("generate aes key")
	key, err := aes.GenerateKey(aes.AES128)
	if err != nil {
		uc.Logger.Error("generate aes key failed", zap.Error(err))
		return nil, fmt.Errorf("generate aes key failed: %v", err)
	}

	uc.Logger.Info("generate aes key succeeded", zap.ByteString("key", key))

	return key, nil
}

func (uc *CryptographyTestHelper) VerifyNonce(challenge sessionModel.Challenge, organizationPublicKey ec.PublicKey) error {
	organizationNonceSignatureBytes, err := challenge.OrganizationNonceSignature.ToBytes()
	if err != nil {
		uc.Logger.Error("OrganizationNonceSignature ToBytes failed", zap.Error(err))
		return fmt.Errorf("OrganizationNonceSignature ToBytes failed: %v", err)
	}

	organizationPublicKeyPem, err := organizationPublicKey.ToPem()
	if err != nil {
		uc.Logger.Error("organizationPublicKey ToPem failed", zap.Error(err))
		return fmt.Errorf("organizationPublicKey ToPem failed: %v", err)
	}

	uc.Logger.Debug("verify",
		zap.String("ecOrganizationPublicKey", string(organizationPublicKeyPem)),
		zap.String("challenge.Nonce", challenge.Nonce),
		zap.Reflect("challenge.Signature", challenge.OrganizationNonceSignature),
	)
	isVerified := ecdsa.Verify(
		&organizationPublicKey,
		[]byte(challenge.Nonce),
		organizationNonceSignatureBytes,
	)

	if !isVerified {
		uc.Logger.Error("verify nonce failed", zap.Bool("isVerified", isVerified))
		return fmt.Errorf("verify nonce failed")
	}

	uc.Logger.Info("verify nonce succeeded")

	return err
}

func (uc *CryptographyTestHelper) SignNonce(nonce string, appPrivateKey ec.PrivateKey) (base64.Base64String, error) {
	appPrivateKeyPem, err := appPrivateKey.ToPem()
	if err != nil {
		uc.Logger.Error("appPrivateKey ToPem failed", zap.Error(err))
		return "", fmt.Errorf("appPrivateKey ToPem failed: %v", err)
	}

	uc.Logger.Debug("sign",
		zap.String("appPrivateKey", string(appPrivateKeyPem)),
		zap.String("nonce", nonce),
	)
	signature, err := ecdsa.Sign(
		&appPrivateKey,
		[]byte(nonce),
	)
	if err != nil {
		uc.Logger.Error("sign nonce failed", zap.Error(err))
		return "", fmt.Errorf("sign nonce failed: %v", err)
	}

	signatureString := base64.ParseFromBytes(signature)

	uc.Logger.Info("sign nonce succeeded", zap.Reflect("signatureString", signatureString))

	return signatureString, nil
}

func (uc *CryptographyTestHelper) EncryptChallengeResult(challengeResult sessionModel.ChallengeResult, organizationPublicKey ec.PublicKey) (base64.Base64String, error) {
	uc.Logger.Debug("json.Marshal", zap.Reflect("challengeResult", challengeResult))
	challengeResultAsJson, err := json.Marshal(challengeResult)
	if err != nil {
		return "", fmt.Errorf("json.Marshal challengeResult failed: %v", err)
	}

	organizationPublicKeyPem, err := organizationPublicKey.ToPem()
	if err != nil {
		uc.Logger.Error("organizationPublicKey ToPem failed", zap.Error(err))
		return "", fmt.Errorf("organizationPublicKey ToPem failed: %v", err)
	}

	uc.Logger.Debug("encrypt",
		zap.String("organizationPublicKey", string(organizationPublicKeyPem)),
		zap.String("challengeResult", string(challengeResultAsJson)),
	)
	encryptedChallengeResult, err := ecies.Encrypt(
		&organizationPublicKey,
		challengeResultAsJson,
	)
	if err != nil {
		uc.Logger.Error("encrypt challengeResult failed", zap.Error(err))
		return "", fmt.Errorf("encrypt challengeResult failed: %v", err)
	}

	encryptedChallengeResultString := base64.ParseFromBytes(encryptedChallengeResult)

	uc.Logger.Info("encrypt challengeResult succeeded", zap.Reflect("encryptedChallengeResult", encryptedChallengeResultString))

	return encryptedChallengeResultString, nil
}

func (uc *CryptographyTestHelper) DecryptSessionToken(sessionAesKey []byte, encryptedSessionToken base64.Base64String) (string, error) {
	uc.Logger.Debug("ToBytes",
		zap.Reflect("encryptedSessionToken", encryptedSessionToken),
	)
	encryptedSessionTokenBytes, err := encryptedSessionToken.ToBytes()
	if err != nil {
		uc.Logger.Error("encryptedSessionToken ToBytes failed", zap.Error(err))
		return "", fmt.Errorf("encryptedSessionToken ToBytes failed: %v", err)
	}

	uc.Logger.Debug("Decrypt",
		zap.Reflect("sessionAesKey", base64.ParseFromBytes(sessionAesKey)),
		zap.Reflect("encryptedSessionToken", encryptedSessionToken),
	)
	sessionToken, err := aes.Decrypt(sessionAesKey, encryptedSessionTokenBytes)
	if err != nil {
		uc.Logger.Error("aes Decrypt failed", zap.Error(err))
		return "", fmt.Errorf("aes Decrypt failed: %v", err)
	}

	return string(sessionToken), nil
}

func (uc *CryptographyTestHelper) EncryptAppIdentity(appIdentity appManagementProcessModel.AppIdentity, sessionAesKey []byte) (base64.Base64String, error) {
	uc.Logger.Debug("json.Marshal", zap.Reflect("appIdentity", appIdentity))
	appIdentityAsJson, err := json.Marshal(appIdentity)
	if err != nil {
		uc.Logger.Error("json.Marshal appIdentity failed", zap.Error(err))
		return "", fmt.Errorf("json.Marshal appIdentity failed: %v", err)
	}

	uc.Logger.Debug("encrypt",
		zap.ByteString("sessionAesKey", sessionAesKey),
		zap.String("appIdentity", string(appIdentityAsJson)),
	)
	encryptedAppIdentity, err := aes.Encrypt(
		sessionAesKey,
		appIdentityAsJson,
	)
	if err != nil {
		uc.Logger.Error("encrypt AppIdentity failed", zap.Error(err))
		return "", fmt.Errorf("encrypt AppIdentity failed: %v", err)
	}

	encryptedAppIdentityString := base64.ParseFromBytes(encryptedAppIdentity)

	uc.Logger.Info("encrypt AppIdentity succeeded", zap.Reflect("encryptedAppIdentity", encryptedAppIdentityString))

	return encryptedAppIdentityString, nil
}

func (uc *CryptographyTestHelper) DecryptCertificate(encryptedCertificate base64.Base64String, sessionAesKey []byte) (*appManagementProcessModel.Certificate, error) {
	decodedEncryptedCertificate, err := base64.Base64String(encryptedCertificate).ToBytes()
	if err != nil {
		uc.Logger.Error("decode encryptedCertificate failed", zap.Error(err))
		return nil, fmt.Errorf("decode encryptedCertificate failed: %v", err)
	}

	uc.Logger.Debug("decrypt",
		zap.Reflect("sessionAesKey", base64.ParseFromBytes(sessionAesKey)),
		zap.String("decodedEncryptedCertificate", string(decodedEncryptedCertificate)),
	)
	decryptedCertificate, err := aes.Decrypt(
		sessionAesKey,
		decodedEncryptedCertificate,
	)
	if err != nil {
		uc.Logger.Error("decrypt Certificate failed", zap.Error(err))
		return nil, fmt.Errorf("decrypt Certificate failed: %v", err)
	}

	uc.Logger.Debug("json.Unmarshal",
		zap.String("decryptedCertificate", string(decryptedCertificate)),
	)
	var certificate appManagementProcessModel.Certificate
	err = json.Unmarshal([]byte(decryptedCertificate), &certificate)
	if err != nil {
		uc.Logger.Error("json.Unmarshal decryptedCertificate failed", zap.Error(err))
		return nil, fmt.Errorf("json.Unmarshal decryptedCertificate failed: %v", err)
	}

	uc.Logger.Info("decrypt Certificate succeeded")
	// disabled due to bytes log , zap.Reflect("certificate", certificate))

	return &certificate, nil
}

func (uc *CryptographyTestHelper) SignDocument(document citizenFinancialClaimModel.Document, appPrivateKey ec.PrivateKey) (base64.Base64String, error) {
	uc.Logger.Debug("json.Marshal", zap.Reflect("document", document))
	documentAsJson, err := json.Marshal(document)
	if err != nil {
		uc.Logger.Error("json.Marshal document failed", zap.Error(err))
		return "", fmt.Errorf("json.Marshal document failed: %v", err)
	}

	appPrivateKeyPem, err := appPrivateKey.ToPem()
	if err != nil {
		uc.Logger.Error("appPrivateKey ToPem failed", zap.Error(err))
		return "", fmt.Errorf("appPrivateKey ToPem failed: %v", err)
	}

	uc.Logger.Debug("sign",
		zap.String("appPrivateKey", string(appPrivateKeyPem)),
		zap.String("document", string(documentAsJson)),
	)
	signature, err := ecdsa.Sign(
		&appPrivateKey,
		documentAsJson,
	)
	if err != nil {
		uc.Logger.Error("sign document failed", zap.Error(err))
		return "", fmt.Errorf("sign document failed: %v", err)
	}

	signatureString := base64.ParseFromBytes(signature)

	uc.Logger.Info("sign document succeeded", zap.Reflect("signature", signatureString))

	return signatureString, nil
}

func (uc *CryptographyTestHelper) EncryptEnvelope(envelope citizenFinancialClaimModel.Envelope, sessionAesKey []byte) (base64.Base64String, error) {
	uc.Logger.Debug("json.Marshal envelope")
	// disabled due to bytes log , zap.Reflect("certificate", certificate))
	envelopeAsJson, err := json.Marshal(envelope)
	if err != nil {
		uc.Logger.Error("json.Marshal envelope failed", zap.Error(err))
		return "", fmt.Errorf("json.Marshal envelope failed: %v", err)
	}

	uc.Logger.Debug("encrypt",
		zap.Reflect("sessionAesKey", base64.ParseFromBytes(sessionAesKey)),
		zap.String("envelope", string(envelopeAsJson)),
	)
	encryptedEnvelope, err := aes.Encrypt(
		sessionAesKey,
		envelopeAsJson,
	)
	if err != nil {
		uc.Logger.Error("encrypt Envelope failed", zap.Error(err))
		return "", fmt.Errorf("encrypt Envelope failed: %v", err)
	}

	encryptedEnvelopeString := base64.ParseFromBytes(encryptedEnvelope)

	uc.Logger.Info("encrypt Envelope succeeded", zap.Reflect("encryptedEnvelope", encryptedEnvelopeString))

	return encryptedEnvelopeString, nil
}

func (uc *CryptographyTestHelper) DecryptFinancialClaimsInformationDocument(encryptedFinancialClaimsInformationDocument base64.Base64String, sessionAesKey []byte) (*sourceSystemModel.FinancialClaimsInformationDocument, error) {
	decodedEncryptedFinancialClaimsInformationDocument, err := base64.Base64String(encryptedFinancialClaimsInformationDocument).ToBytes()
	if err != nil {
		uc.Logger.Error("decode encryptedFinancialClaimsInformationDocument failed", zap.Error(err))
		return nil, fmt.Errorf("decode encryptedFinancialClaimsInformationDocument failed: %v", err)
	}

	uc.Logger.Debug("decrypt",
		zap.Reflect("sessionAesKey", base64.ParseFromBytes(sessionAesKey)),
		zap.String("decodedEncryptedFinancialClaimsInformationDocument", string(decodedEncryptedFinancialClaimsInformationDocument)),
	)
	decryptedFinancialClaimsInformationDocument, err := aes.Decrypt(
		sessionAesKey,
		decodedEncryptedFinancialClaimsInformationDocument,
	)
	if err != nil {
		uc.Logger.Error("failed to decrypt financialClaimsInformation with session aes key", zap.Error(err))
		return nil, fmt.Errorf("failed to decrypt financialClaimsInformation with session aes key: %v", err)
	}

	uc.Logger.Debug("json.Unmarshal decryptedFinancialClaimsInformationDocument",
		zap.String("decryptedFinancialClaimsInformationDocument", string(decryptedFinancialClaimsInformationDocument)),
	)
	var sourceFinancialClaimsInformationDocument sourceSystemModel.FinancialClaimsInformationDocument
	err = json.Unmarshal([]byte(decryptedFinancialClaimsInformationDocument), &sourceFinancialClaimsInformationDocument)
	if err != nil {
		uc.Logger.Error("failed to json unmarshal decryptedFinancialClaimsInformationDocument", zap.Error(err))
		return nil, fmt.Errorf("failed to json unmarshal decryptedFinancialClaimsInformationDocument: %v", err)
	}

	uc.Logger.Info("decrypt FinancialClaimsInformationDocument succeeded", zap.Reflect("sourceFinancialClaimsInformationDocument", sourceFinancialClaimsInformationDocument))

	return &sourceFinancialClaimsInformationDocument, nil
}

func (uc *CryptographyTestHelper) DecryptRegistrationToken(sessionAesKey []byte, encryptedRegistrationToken base64.Base64String) (string, error) {
	uc.Logger.Debug("ToBytes",
		zap.Reflect("encryptedRegistrationToken", encryptedRegistrationToken),
	)
	encryptedRegistrationTokenBytes, err := encryptedRegistrationToken.ToBytes()
	if err != nil {
		uc.Logger.Error("encryptedRegistrationToken ToBytes failed", zap.Error(err))
		return "", fmt.Errorf("encryptedRegistrationToken ToBytes failed: %v", err)
	}

	uc.Logger.Debug("Decrypt",
		zap.Reflect("sessionAesKey", base64.ParseFromBytes(sessionAesKey)),
		zap.Reflect("encryptedRegistrationToken", encryptedRegistrationToken),
	)
	registrationToken, err := aes.Decrypt(sessionAesKey, encryptedRegistrationTokenBytes)
	if err != nil {
		uc.Logger.Error("aes Decrypt failed", zap.Error(err))
		return "", fmt.Errorf("aes Decrypt failed: %v", err)
	}

	return string(registrationToken), nil
}

func (uc *CryptographyTestHelper) DecryptConfigurationToken(sessionAesKey []byte, encryptedConfigurationToken base64.Base64String) (string, error) {
	uc.Logger.Debug("ToBytes",
		zap.Reflect("encryptedConfigurationToken", encryptedConfigurationToken),
	)
	encryptedConfigurationTokenBytes, err := encryptedConfigurationToken.ToBytes()
	if err != nil {
		uc.Logger.Error("encryptedConfigurationToken ToBytes failed", zap.Error(err))
		return "", fmt.Errorf("encryptedConfigurationToken ToBytes failed: %v", err)
	}

	uc.Logger.Debug("Decrypt",
		zap.Reflect("sessionAesKey", base64.ParseFromBytes(sessionAesKey)),
		zap.Reflect("encryptedConfigurationToken", encryptedConfigurationToken),
	)
	configurationToken, err := aes.Decrypt(sessionAesKey, encryptedConfigurationTokenBytes)
	if err != nil {
		uc.Logger.Error("aes Decrypt failed", zap.Error(err))
		return "", fmt.Errorf("aes Decrypt failed: %v", err)
	}

	return string(configurationToken), nil
}

func (uc *CryptographyTestHelper) SignRegistrationToken(registrationToken string, appPrivateKey ec.PrivateKey) (base64.Base64String, error) {
	appPrivateKeyPem, err := appPrivateKey.ToPem()
	if err != nil {
		uc.Logger.Error("appPrivateKey ToPem failed", zap.Error(err))
		return "", fmt.Errorf("appPrivateKey ToPem failed: %v", err)
	}

	uc.Logger.Debug("sign",
		zap.String("appPrivateKey", string(appPrivateKeyPem)),
		zap.String("registrationToken", registrationToken),
	)
	signature, err := ecdsa.Sign(
		&appPrivateKey,
		[]byte(registrationToken),
	)
	if err != nil {
		uc.Logger.Error("sign registrationToken failed", zap.Error(err))
		return "", fmt.Errorf("sign registrationToken failed: %v", err)
	}

	signatureString := base64.ParseFromBytes(signature)

	uc.Logger.Info("sign registrationToken succeeded", zap.Reflect("signature", signatureString))

	return signatureString, nil
}
