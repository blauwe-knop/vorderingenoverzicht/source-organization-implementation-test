// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package helpers

import (
	"fmt"

	"github.com/golang-jwt/jwt/v5"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ec"
)

type JwtTestHelper struct {
	Logger *zap.Logger
}

func NewJwtTestHelper(logger *zap.Logger) *JwtTestHelper {
	return &JwtTestHelper{
		Logger: logger,
	}
}

func (uc *JwtTestHelper) CreateClaims(
	appPublicKey ec.PublicKey,
	appManagerOin string,
	appManagerPublicKey ec.PublicKey,
	givenName string,
	familyName string,
	bsn string,
	issuedAt int64,
	notBefore int64,
	expirationTime int64,
) (jwt.MapClaims, error) {
	uc.Logger.Debug("make claims",
		zap.Reflect("app_public_key", appPublicKey),
		zap.String("app_manager_oin", appManagerOin),
		zap.Reflect("app_manager_public_key", appManagerPublicKey),
		zap.String("given_name", givenName),
		zap.String("family_name", familyName),
		zap.String("bsn", bsn),
		zap.Int64("iat", issuedAt),
		zap.Int64("nbf", notBefore),
		zap.Int64("exp", expirationTime),
	)

	uc.Logger.Debug("ToPem",
		zap.Reflect("appPublicKey", appPublicKey),
	)
	appPublicKeyPem, err := appPublicKey.ToPem()
	if err != nil {
		uc.Logger.Error("appPublicKey.ToPem failed", zap.Error(err))
		return nil, fmt.Errorf("appPublicKey.ToPem failed: %v", err)
	}

	uc.Logger.Debug("ToPem",
		zap.Reflect("appManagerPublicKey", appManagerPublicKey),
	)
	appManagerPublicKeyPem, err := appManagerPublicKey.ToPem()
	if err != nil {
		uc.Logger.Error("appManagerPublicKey.ToPem failed", zap.Error(err))
		return nil, fmt.Errorf("appManagerPublicKey.ToPem failed: %v", err)
	}

	claims := make(jwt.MapClaims)
	claims["app_public_key"] = appPublicKeyPem
	claims["app_manager_oin"] = appManagerOin
	claims["app_manager_public_key"] = appManagerPublicKeyPem
	claims["given_name"] = givenName
	claims["family_name"] = familyName
	claims["bsn"] = bsn
	claims["iat"] = issuedAt
	claims["nbf"] = notBefore
	claims["exp"] = expirationTime

	uc.Logger.Info("make claims succeeded", zap.Reflect("claims", claims))

	return claims, nil
}

func (uc *JwtTestHelper) CreateAccessToken(claims jwt.MapClaims, appManagerPrivateKey ec.PrivateKey) (string, error) {
	uc.Logger.Debug("create access token",
		zap.Reflect("claims", claims),
		zap.Reflect("appManagerPrivateKey", appManagerPrivateKey),
	)
	accessToken, err := jwt.NewWithClaims(jwt.SigningMethodES256, claims).SignedString(appManagerPrivateKey.ToEcdsa())
	if err != nil {
		uc.Logger.Error("create access token failed", zap.Error(err))
		return "", fmt.Errorf("create access token failed: %w", err)
	}

	uc.Logger.Info("create access token succeeded", zap.Reflect("claims", claims))

	return accessToken, nil
}
