// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"time"

	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ec"
	appManagementProcessModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"

	citizenFinancialClaimModel "gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/model"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/source-organization-implementation-test/pkg/helpers"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/source-organization-implementation-test/pkg/repository_log_wrappers"

	"go.uber.org/zap"
)

type SourceOrganizationImplementationTestUsecase struct {
	Logger *zap.Logger
}

func NewSourceOrganizationImplementationTestUsecase(
	logger *zap.Logger,
) *SourceOrganizationImplementationTestUsecase {
	return &SourceOrganizationImplementationTestUsecase{
		Logger: logger,
	}
}

func (uc *SourceOrganizationImplementationTestUsecase) OnlineTest(discoveryUrl string) []error {
	errorList := []error{}

	serviceDiscoveryTestHelper := repository_log_wrappers.NewServiceDiscoveryTestHelper(uc.Logger, discoveryUrl)

	discoveredServices, err := serviceDiscoveryTestHelper.FetchDiscoveredServices()
	if err != nil {
		return []error{err}
	}

	err = serviceDiscoveryTestHelper.CheckDiscoveredServices(discoveredServices)
	if err != nil {
		return []error{err}
	}

	err = serviceDiscoveryTestHelper.CheckSessionApiV1(discoveredServices)
	if err != nil {
		errorList = append(errorList, err)
	} else {
		sessionTestHelper := repository_log_wrappers.NewSessionTestHelper(uc.Logger, discoveredServices.SessionApi.V1)

		err = sessionTestHelper.CheckSessionApiHealth()
		if err != nil {
			errorList = append(errorList, err)
		}
	}

	err = serviceDiscoveryTestHelper.CheckFinancialClaimRequestApiV3(discoveredServices)
	if err != nil {
		errorList = append(errorList, err)
	} else {
		citizenFinancialClaimRepositoryLogWrapper := repository_log_wrappers.NewCitizenFinancialClaimRepositoryLogWrapper(uc.Logger, discoveredServices.FinancialClaimRequestApi.V3)

		err = citizenFinancialClaimRepositoryLogWrapper.CheckFinancialClaimRequestApiHealth()
		if err != nil {
			errorList = append(errorList, err)
		}
	}

	return errorList
}

func (uc *SourceOrganizationImplementationTestUsecase) HealthCheckTest(discoveryUrl string) []error {
	errorList := []error{}

	serviceDiscoveryTestHelper := repository_log_wrappers.NewServiceDiscoveryTestHelper(uc.Logger, discoveryUrl)

	discoveredServices, err := serviceDiscoveryTestHelper.FetchDiscoveredServices()
	if err != nil {
		return []error{err}
	}

	err = serviceDiscoveryTestHelper.CheckDiscoveredServices(discoveredServices)
	if err != nil {
		return []error{err}
	}

	err = serviceDiscoveryTestHelper.CheckSessionApiV1(discoveredServices)
	if err != nil {
		errorList = append(errorList, err)
	} else {
		sessionTestHelper := repository_log_wrappers.NewSessionTestHelper(uc.Logger, discoveredServices.SessionApi.V1)

		_, err = sessionTestHelper.CheckSessionApiHealthCheck()
		if err != nil {
			errorList = append(errorList, err)
		}
	}

	err = serviceDiscoveryTestHelper.CheckFinancialClaimRequestApiV3(discoveredServices)
	if err != nil {
		errorList = append(errorList, err)
	} else {
		citizenFinancialClaimRepositoryLogWrapper := repository_log_wrappers.NewCitizenFinancialClaimRepositoryLogWrapper(uc.Logger, discoveredServices.FinancialClaimRequestApi.V3)

		_, err = citizenFinancialClaimRepositoryLogWrapper.CheckFinancialClaimRequestApiHealthCheck()
		if err != nil {
			errorList = append(errorList, err)
		}
	}
	return errorList
}

func (uc *SourceOrganizationImplementationTestUsecase) FunctionalOfflineTest(
	discoveryUrl string,
	bsn string,
	publicKey string,
	schemeDiscoveryUrl string,
	appManagerOin string,
	appManagerPrivateKey string,
) error {
	ecPublicKey, err := ec.ParsePublicKeyFromPem([]byte(publicKey))
	if err != nil {
		return err
	}

	appManagerEcPrivateKey, err := ec.ParsePrivateKeyFromPem([]byte(appManagerPrivateKey))
	if err != nil {
		return err
	}

	schemeTestHelper := repository_log_wrappers.NewSchemeTestHelper(uc.Logger, schemeDiscoveryUrl)

	appManager, err := schemeTestHelper.FetchAppManager(appManagerOin)
	if err != nil {
		return err
	}

	cryptographyTestHelper := helpers.NewCryptographyTestHelper(uc.Logger)

	appKeyPair, err := cryptographyTestHelper.GenerateEcKeyPair()
	if err != nil {
		return err
	}

	jwtTestHelper := helpers.NewJwtTestHelper(uc.Logger)

	now := time.Now().UTC()

	leewayNotBeforeMinutes := -2

	certificateExpirationMinutes := 5

	appManagerPublicKey, err := ec.ParsePublicKeyFromPem([]byte(appManager.PublicKey))
	if err != nil {
		return err
	}

	claims, err := jwtTestHelper.CreateClaims(
		appKeyPair.PublicKey,
		appManagerOin,
		*appManagerPublicKey,
		"John",
		"Doe",
		bsn,
		now.Unix(),
		now.Add(time.Minute*time.Duration(leewayNotBeforeMinutes)).Unix(),
		now.Add(time.Minute*time.Duration(certificateExpirationMinutes)).Unix(),
	)
	if err != nil {
		return err
	}

	accessToken, err := jwtTestHelper.CreateAccessToken(claims, *appManagerEcPrivateKey)
	if err != nil {
		return err
	}

	certificate := appManagementProcessModel.Certificate{
		Type:  "AppManagerJWTCertificate",
		Value: []byte(accessToken),
	}

	serviceDiscoveryTestHelper := repository_log_wrappers.NewServiceDiscoveryTestHelper(uc.Logger, discoveryUrl)

	discoveredServices, err := serviceDiscoveryTestHelper.FetchDiscoveredServices()
	if err != nil {
		return err
	}

	err = serviceDiscoveryTestHelper.CheckDiscoveredServices(discoveredServices)
	if err != nil {
		return err
	}

	err = serviceDiscoveryTestHelper.CheckSessionApiV1(discoveredServices)
	if err != nil {
		return err
	}

	sessionTestHelper := repository_log_wrappers.NewSessionTestHelper(uc.Logger, discoveredServices.SessionApi.V1)

	sessionAesKey, err := cryptographyTestHelper.GenerateAesKeyPair()
	if err != nil {
		return err
	}

	sessionToken, err := helpers.StartSession(
		cryptographyTestHelper,
		sessionTestHelper,
		appKeyPair.PublicKey,
		*appKeyPair,
		sessionAesKey,
		*ecPublicKey,
		&certificate,
	)
	if err != nil {
		return err
	}

	document := citizenFinancialClaimModel.Document{
		Type: citizenFinancialClaimModel.DocumentType{
			Name: "FINANCIAL_CLAIMS_INFORMATION_REQUEST",
		},
	}

	documentSignature, err := cryptographyTestHelper.SignDocument(
		document,
		*appKeyPair,
	)
	if err != nil {
		return err
	}

	envelope := citizenFinancialClaimModel.Envelope{
		Document:          document,
		DocumentSignature: string(documentSignature),
		Certificate:       certificate.Value,
		CertificateType:   certificate.Type,
	}

	err = serviceDiscoveryTestHelper.CheckFinancialClaimRequestApiV3(discoveredServices)
	if err != nil {
		return err
	}

	citizenFinancialClaimRepositoryLogWrapper := repository_log_wrappers.NewCitizenFinancialClaimRepositoryLogWrapper(
		uc.Logger,
		discoveredServices.FinancialClaimRequestApi.V3,
	)

	encryptedEnvelope, err := cryptographyTestHelper.EncryptEnvelope(
		envelope,
		sessionAesKey,
	)
	if err != nil {
		return err
	}

	encryptedConfigurationToken, err := citizenFinancialClaimRepositoryLogWrapper.ConfigureClaimsRequest(sessionToken, encryptedEnvelope)

	if err != nil {
		return err
	}

	configurationToken, err := cryptographyTestHelper.DecryptConfigurationToken(sessionAesKey, encryptedConfigurationToken)
	if err != nil {
		return err
	}

	encryptedFinancialClaimsInformationDocument, err := citizenFinancialClaimRepositoryLogWrapper.RequestFinancialClaimsInformation(sessionToken, configurationToken)

	if err != nil {
		return err
	}

	_, err = cryptographyTestHelper.DecryptFinancialClaimsInformationDocument(
		encryptedFinancialClaimsInformationDocument,
		sessionAesKey,
	)
	if err != nil {
		return err
	}

	return nil
}

func (uc *SourceOrganizationImplementationTestUsecase) FunctionalOnlineTest(
	discoveryUrl string,
	bsn string,
	publicKey string,
	schemeDiscoveryUrl string,
	appManagerOin string,
) error {
	ecPublicKey, err := ec.ParsePublicKeyFromPem([]byte(publicKey))
	if err != nil {
		return err
	}

	schemeTestHelper := repository_log_wrappers.NewSchemeTestHelper(uc.Logger, schemeDiscoveryUrl)

	appManager, err := schemeTestHelper.FetchAppManager(appManagerOin)
	if err != nil {
		return err
	}

	uc.Logger.Info("setup appManagerServiceDiscoveryTestHelper")
	appManagerServiceDiscoveryTestHelper := repository_log_wrappers.NewServiceDiscoveryTestHelper(
		uc.Logger,
		appManager.DiscoveryUrl,
	)

	appManagerDiscoveredServices, err := appManagerServiceDiscoveryTestHelper.FetchDiscoveredServices()
	if err != nil {
		return err
	}

	err = appManagerServiceDiscoveryTestHelper.CheckDiscoveredServices(appManagerDiscoveredServices)
	if err != nil {
		return err
	}

	cryptographyTestHelper := helpers.NewCryptographyTestHelper(uc.Logger)

	appKeyPair, err := cryptographyTestHelper.GenerateEcKeyPair()
	if err != nil {
		return err
	}

	appPublicKeyPem, err := appKeyPair.PublicKey.ToPem()
	if err != nil {
		return err
	}

	sessionAesKey, err := cryptographyTestHelper.GenerateAesKeyPair()
	if err != nil {
		return err
	}

	uc.Logger.Debug("setup appManagerSessionTestHelper")
	appManagerSessionTestHelper := repository_log_wrappers.NewSessionTestHelper(
		uc.Logger,
		appManagerDiscoveredServices.SessionApi.V1,
	)

	appManagerPublicKey, err := ec.ParsePublicKeyFromPem([]byte(appManager.PublicKey))
	if err != nil {
		return err
	}

	uc.Logger.Debug("appManager startSession")
	appManagerSessionToken, err := helpers.StartSession(
		cryptographyTestHelper,
		appManagerSessionTestHelper,
		appKeyPair.PublicKey,
		*appKeyPair,
		sessionAesKey,
		*appManagerPublicKey,
		&appManagementProcessModel.Certificate{
			Type:  "CertificateTypeNone",
			Value: nil,
		},
	)
	if err != nil {
		return err
	}

	err = appManagerServiceDiscoveryTestHelper.CheckSessionApiV1(appManagerDiscoveredServices)
	if err != nil {
		return err
	}

	err = appManagerServiceDiscoveryTestHelper.CheckRegistrationApiV1(appManagerDiscoveredServices)
	if err != nil {
		return err
	}

	appManagementRepositoryLogWrapper := repository_log_wrappers.NewAppManagementRepositoryLogWrapper(
		uc.Logger,
		appManagerDiscoveredServices.RegistrationApi.V1,
	)

	appIdentity := appManagementProcessModel.AppIdentity{
		AppPublicKey: string(appPublicKeyPem),
		ClientId:     "source_organization_implementation_test",
	}

	encryptedAppIdentity, err := cryptographyTestHelper.EncryptAppIdentity(appIdentity, sessionAesKey)
	if err != nil {
		return err
	}

	registrationToken, err := appManagementRepositoryLogWrapper.RegisterApp(
		appManagerSessionToken,
		encryptedAppIdentity,
	)
	if err != nil {
		return err
	}

	decryptedRegistrationToken, err := cryptographyTestHelper.DecryptRegistrationToken(
		sessionAesKey,
		registrationToken,
	)
	if err != nil {
		return err
	}

	defer func() {
		err = helpers.UnregisterApp(
			appManagementRepositoryLogWrapper,
			cryptographyTestHelper,
			appManagerSessionToken,
			*appKeyPair,
			decryptedRegistrationToken,
		)
	}()

	userIdentity := appManagementProcessModel.UserIdentity{
		Bsn: bsn,
	}

	_, err = appManagementRepositoryLogWrapper.LinkUserIdentity(
		decryptedRegistrationToken,
		userIdentity,
	)
	if err != nil {
		return err
	}

	uc.Logger.Debug("appManager startSession")
	appManagerSessionToken, err = helpers.StartSession(
		cryptographyTestHelper,
		appManagerSessionTestHelper,
		appKeyPair.PublicKey,
		*appKeyPair,
		sessionAesKey,
		*appManagerPublicKey,
		&appManagementProcessModel.Certificate{
			Type:  "CertificateTypeNone",
			Value: nil,
		},
	)
	if err != nil {
		return err
	}

	encryptedCertificate, err := appManagementRepositoryLogWrapper.FetchCertificate(
		appManagerSessionToken,
		decryptedRegistrationToken,
	)
	if err != nil {
		return err
	}

	certificate, err := cryptographyTestHelper.DecryptCertificate(
		encryptedCertificate,
		sessionAesKey,
	)
	if err != nil {
		return err
	}

	serviceDiscoveryTestHelper := repository_log_wrappers.NewServiceDiscoveryTestHelper(uc.Logger, discoveryUrl)

	discoveredServices, err := serviceDiscoveryTestHelper.FetchDiscoveredServices()
	if err != nil {
		return err
	}

	err = serviceDiscoveryTestHelper.CheckDiscoveredServices(discoveredServices)
	if err != nil {
		return err
	}

	err = serviceDiscoveryTestHelper.CheckSessionApiV1(discoveredServices)
	if err != nil {
		return err
	}

	sessionTestHelper := repository_log_wrappers.NewSessionTestHelper(uc.Logger, discoveredServices.SessionApi.V1)

	sessionToken, err := helpers.StartSession(
		cryptographyTestHelper,
		sessionTestHelper,
		appKeyPair.PublicKey,
		*appKeyPair,
		sessionAesKey,
		*ecPublicKey,
		certificate,
	)
	if err != nil {
		return err
	}

	document := citizenFinancialClaimModel.Document{
		Type: citizenFinancialClaimModel.DocumentType{
			Name: "FINANCIAL_CLAIMS_INFORMATION_REQUEST",
		},
	}

	documentSignature, err := cryptographyTestHelper.SignDocument(
		document,
		*appKeyPair,
	)
	if err != nil {
		return err
	}

	envelope := citizenFinancialClaimModel.Envelope{
		Document:          document,
		DocumentSignature: string(documentSignature),
		Certificate:       certificate.Value,
		CertificateType:   certificate.Type,
	}

	err = serviceDiscoveryTestHelper.CheckFinancialClaimRequestApiV3(discoveredServices)
	if err != nil {
		return err
	}

	citizenFinancialClaimRepositoryLogWrapper := repository_log_wrappers.NewCitizenFinancialClaimRepositoryLogWrapper(uc.Logger, discoveredServices.FinancialClaimRequestApi.V3)

	encryptedEnvelope, err := cryptographyTestHelper.EncryptEnvelope(
		envelope,
		sessionAesKey,
	)
	if err != nil {
		return err
	}

	encryptedConfigurationToken, err := citizenFinancialClaimRepositoryLogWrapper.ConfigureClaimsRequest(sessionToken, encryptedEnvelope)
	if err != nil {
		return err
	}

	configurationToken, err := cryptographyTestHelper.DecryptConfigurationToken(sessionAesKey, encryptedConfigurationToken)
	if err != nil {
		return err
	}

	encryptedFinancialClaimsInformationDocument, err := citizenFinancialClaimRepositoryLogWrapper.RequestFinancialClaimsInformation(sessionToken, configurationToken)
	if err != nil {
		return err
	}

	_, err = cryptographyTestHelper.DecryptFinancialClaimsInformationDocument(
		encryptedFinancialClaimsInformationDocument,
		sessionAesKey,
	)
	if err != nil {
		return err
	}

	return nil
}
