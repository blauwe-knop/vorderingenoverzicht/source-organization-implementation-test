// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repository_log_wrappers

import (
	"fmt"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/model"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/repositories"
	"go.uber.org/zap"
)

type SessionTestHelper struct {
	SessionRepository repositories.SessionServiceRepository
	Logger            *zap.Logger
}

func NewSessionTestHelper(logger *zap.Logger, sessionApiUrl string) *SessionTestHelper {
	logger.Debug("create sessionRepository", zap.String("sessionApiUrl", sessionApiUrl))
	sessionRepository := repositories.NewSessionClient(
		sessionApiUrl,
	)

	return &SessionTestHelper{
		Logger:            logger,
		SessionRepository: sessionRepository,
	}
}

func (uc *SessionTestHelper) RequestChallenge() (*model.Challenge, error) {
	uc.Logger.Debug("SessionRepository.RequestChallenge")
	challenge, err := uc.SessionRepository.RequestChallenge()
	if err != nil {
		uc.Logger.Error("SessionRepository.RequestChallenge failed", zap.Error(err))
		return nil, fmt.Errorf("SessionRepository.RequestChallenge failed: %v", err)
	}

	uc.Logger.Info("sessionRepository.RequestChallenge succeeded", zap.Reflect("challenge", challenge))

	return challenge, nil
}

func (uc *SessionTestHelper) CompleteChallenge(challengeResponse model.ChallengeResponse) (base64.Base64String, error) {
	uc.Logger.Debug("SessionRepository.CompleteChallenge", zap.Reflect("challengeResponse", challengeResponse))
	encryptedSessionToken, err := uc.SessionRepository.CompleteChallenge(
		challengeResponse,
	)
	if err != nil {
		uc.Logger.Error("SessionRepository.CompleteChallenge failed", zap.Error(err))
		return "", fmt.Errorf("SessionRepository.CompleteChallenge failed: %v", err)
	}

	uc.Logger.Info("sessionRepository.CompleteChallenge succeeded", zap.Reflect("encryptedSessionToken", encryptedSessionToken))

	return encryptedSessionToken, nil
}

func (uc *SessionTestHelper) CheckSessionApiHealth() error {
	uc.Logger.Debug("sessionRepository.GetHealth")
	err := uc.SessionRepository.GetHealth()
	if err != nil {
		uc.Logger.Error("sessionRepository.GetHealth failed", zap.Error(err))
		return fmt.Errorf("sessionRepository.GetHealth failed: %v", err)
	}

	uc.Logger.Info("sessionRepository.GetHealth succeeded")

	return nil
}

func (uc *SessionTestHelper) CheckSessionApiHealthCheck() (healthcheck.Result, error) {
	uc.Logger.Debug("sessionRepository.GetHealthCheck")
	healthCheckResult := uc.SessionRepository.GetHealthCheck()
	if healthCheckResult.Status == healthcheck.StatusError {
		uc.Logger.Error("sessionRepository.GetHealthCheck failed", zap.Reflect("healthCheckResult", healthCheckResult))
		return healthCheckResult, fmt.Errorf("sessionRepository.GetHealthCheck failed: %v", healthCheckResult)
	}

	uc.Logger.Info("sessionRepository.GetHealthCheck succeeded", zap.Reflect("healthCheckResult", healthCheckResult))

	return healthCheckResult, nil
}
