// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repository_log_wrappers

import (
	"fmt"

	"gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"
	processModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/repositories"
	serviceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/model"
	"go.uber.org/zap"
)

type AppManagementRepositoryLogWrapper struct {
	Logger                         *zap.Logger
	AppManagementProcessRepository repositories.AppManagementProcessRepository
}

func NewAppManagementRepositoryLogWrapper(logger *zap.Logger, registrationApiUrl string) *AppManagementRepositoryLogWrapper {
	logger.Debug("create appManagementProcessRepository", zap.String("registrationApiUrl", registrationApiUrl))
	appManagementProcessRepository := repositories.NewAppManagementProcessClient(
		registrationApiUrl,
	)

	return &AppManagementRepositoryLogWrapper{
		Logger:                         logger,
		AppManagementProcessRepository: appManagementProcessRepository,
	}
}

func (uc *AppManagementRepositoryLogWrapper) RegisterApp(appManagerSessionToken string, encryptedAppIdentity base64.Base64String) (base64.Base64String, error) {
	uc.Logger.Debug("appManagementProcessRepository.RegisterApp",
		zap.String("appManagerSessionToken", appManagerSessionToken),
		zap.Reflect("encryptedAppIdentity", encryptedAppIdentity),
	)
	registrationToken, err := uc.AppManagementProcessRepository.RegisterApp(
		appManagerSessionToken,
		encryptedAppIdentity,
	)
	if err != nil {
		uc.Logger.Error("appManagementProcessRepository.RegisterApp failed", zap.Error(err))
		return "", fmt.Errorf("appManagementProcessRepository.RegisterApp failed: %v", err)
	}

	uc.Logger.Info("appManagementProcessRepository.RegisterApp succeeded", zap.Reflect("registerToken", registrationToken))

	return registrationToken, nil
}

func (uc *AppManagementRepositoryLogWrapper) LinkUserIdentity(registrationToken string, userIdentity processModel.UserIdentity) (*serviceModel.Registration, error) {
	uc.Logger.Debug("appManagementProcessRepository.LinkUserIdentity",
		zap.String("registrationToken", registrationToken),
		zap.Reflect("userIdentity", userIdentity),
	)
	registration, err := uc.AppManagementProcessRepository.LinkUserIdentity(
		registrationToken,
		userIdentity,
	)
	if err != nil {
		uc.Logger.Error("appManagementProcessRepository.LinkUserIdentity failed", zap.Error(err))
		return nil, fmt.Errorf("appManagementProcessRepository.LinkUserIdentity failed: %v", err)
	}

	uc.Logger.Info("AppManagementProcessRepository.LinkUserIdentity succeeded", zap.Reflect("registration", registration))

	return registration, err
}

func (uc *AppManagementRepositoryLogWrapper) FetchCertificate(sessionToken string, registrationToken string) (base64.Base64String, error) {
	uc.Logger.Debug("AppManagementProcessRepository.FetchCertificate",
		zap.String("sessionToken", sessionToken),
		zap.String("registrationToken", registrationToken),
	)
	encryptedCertificate, err := uc.AppManagementProcessRepository.FetchCertificate(
		sessionToken,
		registrationToken,
	)
	if err != nil {
		uc.Logger.Error("AppManagementProcessRepository.FetchCertificate failed", zap.Error(err))
		return "", fmt.Errorf("AppManagementProcessRepository.FetchCertificate failed: %v", err)
	}

	uc.Logger.Info("AppManagementProcessRepository.FetchCertificate succeeded", zap.Reflect("encryptedCertificate", encryptedCertificate))

	return encryptedCertificate, nil
}

func (uc *AppManagementRepositoryLogWrapper) UnregisterApp(appManagerSessionToken string, unregisterAppRequest processModel.UnregisterAppRequest) error {
	uc.Logger.Debug("AppManagementProcessRepository.UnregisterApp",
		zap.String("appManagerSessionToken", appManagerSessionToken),
		zap.Reflect("unregisterAppRequest", unregisterAppRequest),
	)
	err := uc.AppManagementProcessRepository.UnregisterApp(
		appManagerSessionToken,
		unregisterAppRequest,
	)
	if err != nil {
		uc.Logger.Error("AppManagementProcessRepository.UnregisterApp failed", zap.Error(err))
		return fmt.Errorf("AppManagementProcessRepository.UnregisterApp failed: %v", err)
	}

	uc.Logger.Info("AppManagementProcessRepository.UnregisterApp succeeded")

	return nil
}
