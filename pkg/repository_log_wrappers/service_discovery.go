// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repository_log_wrappers

import (
	"fmt"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/service-discovery-process/pkg/model"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/service-discovery-process/pkg/repositories"
	"go.uber.org/zap"
)

type ServiceDiscoveryTestHelper struct {
	ServiceDiscoveryRepository repositories.ServiceDiscoveryRepository
	Logger                     *zap.Logger
}

func NewServiceDiscoveryTestHelper(logger *zap.Logger, discoveryUrl string) *ServiceDiscoveryTestHelper {
	logger.Debug("create serviceDiscoveryRepository", zap.String("discoveryUrl", discoveryUrl))
	serviceDiscoveryRepository := repositories.NewServiceDiscoveryClient(
		discoveryUrl,
	)

	return &ServiceDiscoveryTestHelper{
		Logger:                     logger,
		ServiceDiscoveryRepository: serviceDiscoveryRepository,
	}
}

func (uc *ServiceDiscoveryTestHelper) FetchDiscoveredServices() (*model.DiscoveredServices, error) {
	uc.Logger.Debug("serviceDiscoveryRepository.Fetch")
	discoveredServices, err := uc.ServiceDiscoveryRepository.Fetch()
	if err != nil {
		uc.Logger.Error("ServiceDiscoveryRepository.Fetch failed", zap.Error(err))
		return nil, fmt.Errorf("ServiceDiscoveryRepository.Fetch failed: %v", err)
	}

	uc.Logger.Info("ServiceDiscoveryRepository.Fetch succeeded", zap.Reflect("discoveredServices", discoveredServices))

	return discoveredServices, nil
}

func (uc *ServiceDiscoveryTestHelper) CheckDiscoveredServices(discoveredServices *model.DiscoveredServices) error {
	uc.Logger.Debug("check discoveredServices", zap.Reflect("discoveredServices", discoveredServices))
	if discoveredServices == nil {
		uc.Logger.Error("bk configuration is empty")
		return fmt.Errorf("bk configuration is empty")
	}

	uc.Logger.Info("check discoveredServices succeeded")

	return nil
}

func (uc *ServiceDiscoveryTestHelper) CheckSessionApiV1(discoveredServices *model.DiscoveredServices) error {
	uc.Logger.Debug("check discoveredServices.SessionApi.V1", zap.Reflect("discoveredServices.SessionApi.V1", discoveredServices.SessionApi.V1))
	if discoveredServices.SessionApi.V1 == "" {
		uc.Logger.Error("check discoveredServices.SessionApi.V1 is empty")
		return fmt.Errorf("check discoveredServices.SessionApi.V1 is empty")
	}

	uc.Logger.Info("check discoveredServices.SessionApi.V1 succeeded")

	return nil
}

func (uc *ServiceDiscoveryTestHelper) CheckFinancialClaimRequestApiV3(discoveredServices *model.DiscoveredServices) error {
	uc.Logger.Debug("check discoveredServices.FinancialClaimRequestApi.V3", zap.Reflect("discoveredServices.FinancialClaimRequestApi.V3", discoveredServices.FinancialClaimRequestApi.V3))
	if discoveredServices.FinancialClaimRequestApi.V3 == "" {
		uc.Logger.Error("discoveredServices.FinancialClaimRequestApi.V3 is empty")
		return fmt.Errorf("discoveredServices.FinancialClaimRequestApi.V3 is empty")
	}

	uc.Logger.Info("check discoveredServices.FinancialClaimRequestApi.V3 succeeded")

	return nil
}

func (uc *ServiceDiscoveryTestHelper) CheckRegistrationApiV1(discoveredServices *model.DiscoveredServices) error {
	uc.Logger.Debug("check discoveredServices.RegistrationApi.V1", zap.Reflect("discoveredServices.RegistrationApi.V1", discoveredServices.RegistrationApi.V1))
	if discoveredServices.RegistrationApi.V1 == "" {
		uc.Logger.Error("discoveredServices.RegistrationApi.V1 is empty")
		return fmt.Errorf("discoveredServices.RegistrationApi.V1 is empty")
	}

	uc.Logger.Info("check discoveredServices.RegistrationApi.V1 succeeded")

	return nil
}
